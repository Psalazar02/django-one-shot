from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm

def todo_list(request):
    list = TodoList.objects.all()
    context = {
        "todo_list": list,
    }
    return render(request, "todos/list.html", context)

def show_todo(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,
    }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save(commit=False)
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)

def edit_todolist(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save(commit=False)
            form.save()
            return redirect("todo_list_detail", list.id )
    else:
        form = TodoForm(instance=list)
    context = {
        "list_object": list,
        "list_form": form,
    }
    return render(request, "todos/edit.html", context)

def delete_todolist(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("/todos/")
    return render(request, "todos/delete.html")

def create_todoitem(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            list = form.save(commit=False)
            list.save()
            return redirect("todo_list_detail", id=list.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/create-item.html", context)

def edit_todoitem(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.save()
            return redirect("todo_list_detail", id=task.list.id)
    else:
        form = ItemForm(instance=task)
    context = {
        "task_object": task,
        "task_form": form,
    }
    return render(request, "todos/edit-item.html", context)
