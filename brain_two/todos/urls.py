from django.urls import path
from todos.views import todo_list, show_todo, create_todo, edit_todolist, delete_todolist, create_todoitem, edit_todoitem

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todoitem, name="todo_item_update"),
]
